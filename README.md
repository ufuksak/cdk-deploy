# CDK Serverless

Sample application demonstrating a web application using serverless, React and AWS CDK V2.

## Useful commands

 * `npm i`            install dependencies
 * `npm t`            run tests
 * `npm run deploy`   deploy to your credentialed account
 * `npm cdk diff`     to see the deployment differences
 * `npx cdk destroy`  clean up when you're done
 
## TODO
 * Change Backend part with nestjs
 * Add Hydra function call for login auth

## `cdk deploy` command a sample 
```
Bundling asset CdkServerlessStack/ReadNotesFn/Code/Stage...

  cdk.out/bundling-temp-5ab851701ee00e87fa52db469b3b8d984b53fd26ef45d02e3b372d461f0049aa/index.js  1.2mb ⚠️

⚡ Done in 71ms
Bundling asset CdkServerlessStack/WriteNoteFn/Code/Stage...

  cdk.out/bundling-temp-e80a200f6a366ca4dd2391a88f7c4b94c559ca43a6582c93778d0e63f13a9494/index.js  1.2mb ⚠️

⚡ Done in 59ms
Bundling asset CdkServerlessStack/DeployWebsite/Asset1/Stage...
0.14.8
vite v2.7.7 building for production...
✓ 26 modules transformed.
dist/index.html                  0.74 KiB
dist/assets/index.bdf8a782.js    2.23 KiB / gzip: 0.97 KiB
dist/assets/index.75577bda.css   0.35 KiB / gzip: 0.24 KiB
dist/assets/vendor.9598694a.js   128.50 KiB / gzip: 41.34 KiB

✨  Synthesis time: 10.54s

This deployment will make potentially sensitive changes according to your current security approval level (--require-approval broadening).
Please confirm you intend to make the following modifications:

IAM Statement Changes
┌───┬───────────────────────────┬────────┬─────────────────────────────────────────────────────────┬──────────────────────────────────────────────────────────┬───────────┐
│   │ Resource                  │ Effect │ Action                                                  │ Principal                                                │ Condition │
├───┼───────────────────────────┼────────┼─────────────────────────────────────────────────────────┼──────────────────────────────────────────────────────────┼───────────┤
│ - │ ${NotesTableD0D0D2F1.Arn} │ Allow  │ dynamodb:BatchGetItem                                   │ AWS:${ReadNotesFn/ServiceRole}                           │           │
│   │                           │        │ dynamodb:ConditionCheckItem                             │                                                          │           │
│   │                           │        │ dynamodb:GetItem                                        │                                                          │           │
│   │                           │        │ dynamodb:GetRecords                                     │                                                          │           │
│   │                           │        │ dynamodb:GetShardIterator                               │                                                          │           │
│   │                           │        │ dynamodb:Query                                          │                                                          │           │
│   │                           │        │ dynamodb:Scan                                           │                                                          │           │
│ - │ ${NotesTableD0D0D2F1.Arn} │ Allow  │ dynamodb:BatchWriteItem                                 │ AWS:${WriteNoteFn/ServiceRole}                           │           │
│   │                           │        │ dynamodb:DeleteItem                                     │                                                          │           │
│   │                           │        │ dynamodb:PutItem                                        │                                                          │           │
│   │                           │        │ dynamodb:UpdateItem                                     │                                                          │           │
├───┼───────────────────────────┼────────┼─────────────────────────────────────────────────────────┼──────────────────────────────────────────────────────────┼───────────┤
│ + │ ${Notes.Arn}              │ Allow  │ dynamodb:BatchGetItem                                   │ AWS:${ReadNotesFn/ServiceRole}                           │           │
│   │                           │        │ dynamodb:ConditionCheckItem                             │                                                          │           │
│   │                           │        │ dynamodb:GetItem                                        │                                                          │           │
│   │                           │        │ dynamodb:GetRecords                                     │                                                          │           │
│   │                           │        │ dynamodb:GetShardIterator                               │                                                          │           │
│   │                           │        │ dynamodb:Query                                          │                                                          │           │
│   │                           │        │ dynamodb:Scan                                           │                                                          │           │
│ + │ ${Notes.Arn}              │ Allow  │ dynamodb:BatchWriteItem                                 │ AWS:${WriteNoteFn/ServiceRole}                           │           │
│   │                           │        │ dynamodb:DeleteItem                                     │                                                          │           │
│   │                           │        │ dynamodb:PutItem                                        │                                                          │           │
│   │                           │        │ dynamodb:UpdateItem                                     │                                                          │           │
└───┴───────────────────────────┴────────┴─────────────────────────────────────────────────────────┴──────────────────────────────────────────────────────────┴───────────┘
(NOTE: There may be security-related changes not in this list. See https://github.com/aws/aws-cdk/issues/1299)

Do you wish to deploy these changes (y/n)? 
```
